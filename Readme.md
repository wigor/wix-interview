## Provision servers

        cd scripts/terraform
        terraform apply -var 'vpc_id="....."' -var 'private_subnet_id={ "0"=".....", "1"="......" }' -var 'public_subnet_id={ "0"=".....", "1"="......" }'
        
## Build application

        ACCOUNT_ID=..... VERSION=..... scripts/deployment/build_image.sh
        
## Deploy application

        ACCOUNT_ID=..... VERSION=..... scripts/deployment/deploy.sh