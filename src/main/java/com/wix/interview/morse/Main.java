package com.wix.interview.morse;

import com.wix.interview.morse.encoders.MorseEncoder;
import com.wix.interview.morse.handlers.EchoAddressHandler;
import com.wix.interview.morse.handlers.HealthCheckHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class Main {

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        EventLoopGroup healthcheckGroup = new NioEventLoopGroup(1);
        EventLoopGroup morseAcceptorGroup = new NioEventLoopGroup(1);
        EventLoopGroup morseWorkerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap morseServer = new ServerBootstrap();
            morseServer.group(morseAcceptorGroup, morseWorkerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast("encoder", new MorseEncoder());
                            pipeline.addLast(new EchoAddressHandler());
                        }
                    });

            ChannelFuture morseEndpointFuture = morseServer.bind(8080).sync();

            ServerBootstrap healthCheckServer = new ServerBootstrap();
            healthCheckServer.group(healthcheckGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast(new HealthCheckHandler());
                        }
                    });

            ChannelFuture hcEndpointFuture = healthCheckServer.bind(8081).sync();

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    LOG.info("Shutdown process have been started");
                    LOG.info("Dropping HealthCheck endpoint port");
                    hcEndpointFuture.channel().close().sync();
                    LOG.info("Graceful shutdown");
                    healthcheckGroup.shutdownGracefully(0, 1, TimeUnit.SECONDS);
                    Future<?> morseAcceptorClose = morseAcceptorGroup.shutdownGracefully(5, 60, TimeUnit.SECONDS);
                    Future<?> morseWorkerClose = morseWorkerGroup.shutdownGracefully(5, 60, TimeUnit.SECONDS);

                    morseAcceptorClose.await();
                    morseWorkerClose.await();
                } catch (InterruptedException e) {
                    LOG.error("", e);
                }
            }));

            morseEndpointFuture.channel().closeFuture().sync();
            hcEndpointFuture.channel().closeFuture().sync();
        } finally {
            Future<?> morseAcceptorClose = morseAcceptorGroup.shutdownGracefully();
            Future<?> morseWorkerClose = morseWorkerGroup.shutdownGracefully();

            morseAcceptorClose.await();
            morseWorkerClose.await();
        }

    }

}
