package com.wix.interview.morse.encoders;

import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.string.StringEncoder;

import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class MorseEncoder extends StringEncoder {

    private static String[] digits = new String[]{
            "-----",
            ".----",
            "..---",
            "...--",
            "....-",
            ".....",
            "-....",
            "--...",
            "---..",
            "----."
    };
    private static String dot = "......";


    @Override
    protected void encode(ChannelHandlerContext ctx, CharSequence msg, List<Object> out) throws Exception {
        if (msg.length() == 0) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        msg.chars().forEach(c -> {
            if (c >= 48 && c <= 57) {
                sb.append(digits[c - 48]);
            }
            if (c == '.') {
                sb.append(dot);
            }
        });

        out.add(ByteBufUtil.encodeString(ctx.alloc(), CharBuffer.wrap(sb.toString()), StandardCharsets.US_ASCII));
    }
}
