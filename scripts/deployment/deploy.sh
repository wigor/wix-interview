#!/usr/bin/env bash

CUR_PATH=$(pwd)
BASEDIR=$(dirname "$0")
pushd "${CUR_PATH}/${BASEDIR}"

INSTANCES=$(aws ec2 describe-instances --filter "Name=tag:Name,Values=moreseservice/main" --query "Reservations[*].Instances[*].NetworkInterfaces[*].PrivateIpAddresses[0].PrivateIpAddress"|grep -Eo '[.0-9]+')

for IP in ${INSTANCES}; do
    scp -o StrictHostKeyChecking=no reload_images.sh ec2-user@${IP}:~/
    ssh -o StrictHostKeyChecking=no ec2-user@${IP} bash -c "sudo ACCOUNT=${ACCOUNT} VERSION=${VERSION} ~/reload_images.sh"
done

popd