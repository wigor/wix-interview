#!/usr/bin/env bash

CUR_PATH=$(pwd)
BASEDIR=$(dirname "$0")
pushd "${CUR_PATH}/${BASEDIR}/../.."
docker build -t "${ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/morseservice/server:${VERSION}" .
docker push ${ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/morseservice/server:${VERSION}
popd