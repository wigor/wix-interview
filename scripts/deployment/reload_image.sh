#!/usr/bin/env bash -e

docker stop -t 60 wix-interview || true
docker pull ${ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/morseservice/server:${VERSION}
docker run -p 8080:8080 -p 8081:8081 --name wix-interview ${ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/morseservice/server:${VERSION}
