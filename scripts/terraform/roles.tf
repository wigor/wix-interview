resource "aws_iam_role" "morse-service-role" {
  name               = "morse-service-role"
  path               = "/"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_instance_profile" "im-morse-server" {
  name = "im-morse-server"
  role = "${aws_iam_role.morse-service-role.name}"
}
