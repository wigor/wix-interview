terraform {
  required_version = ">= 0.11"
  backend "local" {
    path = "morseservice.tfstate"
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "ami" { default = "ami-66506c1c" }
variable "key_name" { default = "vpc_keypair_default" }
variable "vpc_id" {}
variable "tfstate-id" { default = "moreseservice" }
variable "instance_type" { default = "t2.micro" }
variable "instance_count" { default = "1" }

variable "private_subnet_id" {}

variable "public_subnet_id" {}

resource "aws_security_group" "moreseservice-instances-sg" {
  name = "${var.tfstate-id}/main"

  tags {
    Name = "${var.tfstate-id}/main"
  }

  vpc_id = "${var.vpc_id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"

    cidr_blocks = [
      "10.0.0.0/16"
    ]
  }

  ingress {
    from_port = 8080
    to_port = 8081
    protocol = "tcp"

    cidr_blocks = [
      "10.0.0.0/16"
    ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_instance" "main" {
  count = "${var.instance_count}"
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}"
  vpc_security_group_ids = [
    "${aws_security_group.moreseservice-instances-sg.id}"
  ]
  subnet_id = "${lookup(var.private_subnet_id, count.index % 2)}"
  iam_instance_profile = "${aws_iam_instance_profile.im-morse-server.name}"

  tags {
    Name = "${var.tfstate-id}/main"
  }

  user_data = "${file("userdata.sh")}"
}