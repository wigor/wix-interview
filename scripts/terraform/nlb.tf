resource "aws_security_group" "nlb-public-sg" {
  name = "nlb-public-sg"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port = 8080
    to_port = 8081
    protocol = "tcp"

    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"

    cidr_blocks = [
      "10.0.0.0/16",
    ]
  }

}

resource "aws_lb" "morse-nlb" {
  name = "morse-nlb"
  internal = false
  security_groups = ["${aws_security_group.nlb-public-sg.id}"]
  subnets = [
    "${var.public_subnet_id["0"]}",
    "${var.public_subnet_id["1"]}"]

  load_balancer_type = "network"
  enable_deletion_protection = true
}

resource "aws_lb_listener" "morse-nlb-listener" {
  load_balancer_arn = "${aws_lb.morse-nlb.arn}"
  port = "8080"
  protocol = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.morse-nlb-tg.arn}"
    type = "forward"
  }
}

resource "aws_lb_target_group" "morse-nlb-tg" {
  name = "morse-nlb-tg"
  port = 8080
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8081
    interval = 5
    healthy_threshold = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_target_group_attachment" "morse-nlb-tg-attachment" {
  count = "${var.instance_count}"
  target_group_arn = "${aws_lb_target_group.morse-nlb-tg.arn}"
  target_id = "${element(aws_instance.main.*.id, count.index)}"
}
