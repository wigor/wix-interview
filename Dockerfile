FROM openjdk:8

RUN apt-get update
RUN apt-get -y install maven

ADD . /src

WORKDIR /src
RUN mvn package

FROM openjdk:8

COPY --from=0 /src/target/wix-interview.jar /opt

EXPOSE 8080
EXPOSE 8081

ENTRYPOINT ["/usr/bin/java", "-jar", "/opt/wix-interview.jar"]